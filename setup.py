from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in dmpi/__init__.py
from dmpi import __version__ as version

setup(
	name='dmpi',
	version=version,
	description='This app',
	author='Yaser Tawash',
	author_email='yaser.tawash@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
