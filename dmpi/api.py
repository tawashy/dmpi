import frappe
from frappe.model.document import Document
import json
from enum import Enum

class RequestStatus(Enum):
    CREATED = 0
    UPDATED = 1


def create_registration_request(data):
    details = data['data']
    try:
        doc = frappe.get_doc({
            'doctype': 'Registration Request',
            'reference_id': details['id'],
            "first_name": details['first_name'],
            "last_name": details['last_name'],
            "email_address": details['email_address'],
            "phone_number": details['phone_number'],
            "mobile_number": details['mobile_number'],
            "idiqama_no": details['idiqama_no'],
            "business_email": details['business_email'],
            "organization_name_en": details['organization_name_en'],
            "organization_name_ar": details['organization_name_ar'],
            "address": details['address'],
            "city": details['city'],
            "domain_name": details['domain_name'],
        })
        doc.insert(ignore_permissions=True)
        frappe.db.commit()
    except:
        frappe.log_error(frappe.get_traceback(),'Failed to update account.')
        return "Something went wrong."

    return {"response_status":200, "message":"Registration Request CREATED!"}

def update_registration_request(data):
    details = data['data']
    try:
        doc = frappe.get_last_doc('Registration Request', filters={"reference_id": details['id']})
        doc.first_name = details['first_name']
        doc.last_name = details['last_name']
        doc.phone_number = details['phone_number']
        doc.mobile_number = details['mobile_number']
        doc.idiqama_no = details['idiqama_no']
        doc.business_email = details['business_email']
        doc.organization_name_en = details['organization_name_en']
        doc.organization_name_ar = details['organization_name_ar']
        doc.address = details['address']
        doc.city = details['city']
        doc.domain_name = details['domain_name']

        doc.save(ignore_permissions=True)
        frappe.db.commit()
        return doc
    except:
        frappe.log_error(frappe.get_traceback(),'Failed to update account.')
        return "Something went wrong."
    return {"response_status":200, "message":"Registration Request UPDATED!"}

def handle_registration_request(data):
    # Check request_status 
    # Created: 0, Updated: 1,
    response = None
    if data['request_status'] == 0:
        print('Create Registration Request')
        response = create_registration_request(data)
        return response

    if data['request_status'] == 1:
        print('update Registration Request')
        response = update_registration_request(data)
        
    return response





def validation(data): 
    return []

@frappe.whitelist(allow_guest=True)
def webhook():
    # Check the request data
    if not frappe.request.data:
        frappe.log_error(message="The request comming from the request does not have data.", title='Webhook Error')
        frappe.local.response['http_status_code'] = 400
        return {"response_status":400, "message":"Error validating request body!"}

    # Validating data ...
    data = json.loads(frappe.request.data)
    errors = validation(data)

    if len(errors) > 0:
        frappe.local.response['http_status_code'] = 400
        return {"response_status":400, "message":"Error validating request body!!"}


    # Request Type:
    request_type = data['request_type']
    response = None

    if request_type == 'registration_request':
        print('Request Type: registration_request')
        response = handle_registration_request(data)

    if request_type == 'customer':
        print('Request Type: customer')

    return response







