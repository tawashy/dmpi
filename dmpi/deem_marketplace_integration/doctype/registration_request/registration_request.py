# Copyright (c) 2021, Yaser Tawash and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
import json

class RegistrationRequest(Document):

	def approve_request(self, doc):
		pass

	def reject_request(self, doc):
		pass

	def update_request(self, data):
		pass

	def before_save(self):
		return frappe.throw('You need to add update organization type first.')

	def before_cancel(self):
		return frappe.throw('This Document cannot be canceld.')

	def before_submit(self): 
		# this should send the request to the microservice by either approving or 
		# rejecting the request. 

		# if status is Approved: send approval to service url. 
		if self.status == "Approved":
			# LOGIC HERE...
			print('SEND TO BE APPROVED...')
			return frappe.throw('Cannot Approve the Request')

		# if status is Rejected: send rejection to service url.
		if self.status == "Rejected":
			# LOGIC HERE....
			print('SEND TO BE Rejected ...')
			return frappe.throw('Cannot Reject the Request')

		# Throw if anything goes wrong...
		return frappe.throw('Something went wrong!!')




	# def on_update(self):
	# 	""" 
	# 	Make sure the request get sent to the microservice.
	# 	insure the data get sent only of the organization type got changed. 
	# 	"""
	# 	# print('UPDATING NEW DOC ... %s', self.is_new())
	# 	# pass
	# 	return frappe.throw('....')





# @frappe.whitelist()
# def approve_request(doc: dict):
# 	details = json.loads(doc)
# 	if 'organization_type' not in details:
# 		return frappe.msgprint(title="ERROR", msg="You need to set organization type before approving.", indicator='red')

# 	current_doc = frappe.get_doc('Registration Request', details['name'])
# 	if not current_doc.organization_type:
# 		return frappe.msgprint(title="ERROR", msg="You need to save the document before updating.", indicator='red')
# 	# update document. 
# 	current_doc.status = 'Approved'
# 	current_doc.save()
# 	# current_doc.organization_type = details['organization_type']
# 	# current_doc.reload()
# 	# frappe.msgprint('Request has been approved. %s'% (doc,), 'Approved Successfuly')


# @frappe.whitelist()
# def reject_request(doc: dict): 
# 	details = json.loads(doc)
# 	# if 'organization_type' not in details:
# 	# 	return frappe.msgprint(title="ERROR", msg="You need to set organization type before approving.", indicator='red')

# 	current_doc = frappe.get_doc('Registration Request', details['name'])
# 	# if not current_doc.organization_type:
# 	# 	return frappe.msgprint(title="ERROR", msg="You need to save the document before updating.", indicator='red')
# 	# update document. 
# 	current_doc.status = 'Rejected'
# 	current_doc.save()
# 	frappe.msgprint('Request has been rejected.', 'Rejected Successfuly')

