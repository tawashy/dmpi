## Deem Marketplace Integration

This app will allow to integrate frappe framework with the current Deem Marketplace Microservices.
Using this app, will make user able to interact with the records in the microservices and use webhooks to make sure all informations are synced between the systems. 

#### License

MIT